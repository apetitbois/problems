# Ceasar's Cipher

Ceasar Cipher: “Julius Caesar protected his confidential information by encrypting it in a cipher. Caesar's cipher rotated every letter in a string by a fixed number, , making it unreadable by his enemies. Given a string, , and a number, , encrypt  and print the resulting string.
Note: The cipher only encrypts letters; symbols, such as -, remain unencrypted”


example input:


example output:
