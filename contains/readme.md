# AnthonyPetitbois in a string

The goal of this exercise is to write a software that will take a string and print:
Yes if AnthonyPetitbois was found inside
No if AnthonyPetitbois was not found inside

Definition of being found inside:
We say that a string, , contains the word AnthonyPetitbois if a subsequence of the characters in  spell the word AnthonyPetitbois. For example, AaaanntthhoonnyyPeettiittbbooiiss does contain AnthonyPetitbois, but AaaannnnttthhhooonnnyPettttiiiittttoooiiisss does not (the characters all appear in the same order, but it's missing a b).
sdfasvsdfAasdnvawsetdgaweshgawsoefdawsengvawseyfacwsePfwesegawsetgfwesifawestgfwasefbw3aowesdtihertjs would also contain AnthonyPetitbois


Sample input:
```
AaaanntthhoonnyyPeettiittbbooiiss
AaaannnnttthhhooonnnyPettttiiiittttoooiiisss
sdfasvsdfAasdnvawsetdgaweshgawsoefdawsengvawseyfacwsePfwesegawsetgfwesifawestgfwasefbw3aowesdtihertjs
```
Sample output:
```
Yes
No
Yes
```
