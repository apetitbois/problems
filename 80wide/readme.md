# 80 character wide terminal

In this exercise, the goal is to write a software that will take as an input an array of strings and writes them
backwards in an 80 wide character window, all right aligned.

Provided input:
```
lines = [
	'William Shakespeare was the son of John Shakespeare, an alderman and a successful glover originally from Snitterfield, and Mary Arden, the daughter of an affluent landowning farmer.',
	'He was born in Stratford-upon-Avon and baptised there on 26 April 1564. ',
	'His actual date of birth remains unknown, but is traditionally observed on 23 April, Saint George's Day.'
	'This date, which can be traced back to an 18th-century scholar's mistake, has proved appealing to biographers because Shakespeare died on 23 April 1616.',
	'He was the third child of eight and the eldest surviving son.',
	'',
	'Although no attendance records for the period survive, most biographers agree that Shakespeare was probably educated at the King's New School in Stratford, a free school chartered in 1553, about a quarter-mile (400 m) from his home.',
}
```

Expected output:
```
ufsseccus a dna namredla na ,eraepsekahS nhoJ fo nos eht saw eraepsekahS mailliW
eulffa na fo rethguad eht ,nedrA yraM dna ,dleifrettinS morf yllanigiro revolg l
                                                           .remraf gninwodnal tn
         .4651 lirpA 62 no ereht desitpab dna novA-nopu-droftartS ni nrob saw eH
pA 32 no devresbo yllanoitidart si tub ,nwonknu sniamer htrib fo etad lautca siH
                                                        .yaD s'egroeG tniaS ,lir
rp sah ,ekatsim s'ralohcs yrutnec-ht81 na ot kcab decart eb nac hcihw ,etad sihT
        .6161 lirpA 32 no deid eraepsekahS esuaceb srehpargoib ot gnilaeppa devo
                   .nos gnivivrus tsedle eht dna thgie fo dlihc driht eht saw eH

ht eerga srehpargoib tsom ,evivrus doirep eht rof sdrocer ecnadnetta on hguohtlA
rf a ,droftartS ni loohcS weN s'gniK eht ta detacude ylbaborp saw eraepsekahS ta
        .emoh sih morf )m 004( elim-retrauq a tuoba ,3551 ni deretrahc loohcs ee
```
